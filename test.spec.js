import { Async } from 'hyphae/async'
import { Sync } from 'hyphae/sync'
import { span } from 'porcini'
import { text } from 'morphogenesis'

console.log(text)

test('foo', () => {
  expect(Sync.from('123').toArray()).toEqual(['123'])
  expect(span('foo').textContent).toEqual('foo')
})
